﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LZL.IISWeb.Core
{
    public class IISWebElementCollection:ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new IISWebElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((IISWebElement) element).ID;
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        } 
        protected override string ElementName
        {
            get { return "iisWeb"; }
        }

        public IISWebElement this[int index]
        {
            get
            {
                return (IISWebElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }  
        }
    }
}