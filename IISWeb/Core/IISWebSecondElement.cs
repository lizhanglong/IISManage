﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LZL.IISWeb.Core
{
    public class IISWebSecondElement : ConfigurationElement
    {
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path
        {
            get { return base["path"] as string; }
            set { base["path"] = value; }
        }

    }
}