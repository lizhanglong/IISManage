﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LZL.IISWeb.Core
{
    public class IISWebSecondElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new IISWebSecondElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((IISWebSecondElement)element).Path;
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        } 
        protected override string ElementName
        {
            get { return "iisWebSecond"; }
        }
    }
}