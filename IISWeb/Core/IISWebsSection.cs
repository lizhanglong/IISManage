﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LZL.IISWeb.Core
{
    public class IISWebsSection : ConfigurationSection
    {
        [ConfigurationProperty("iisWebs")]
        public IISWebElementCollection IISWebs
        {
            get
            {
                return (IISWebElementCollection)base["iisWebs"];
            }
        }
    }
}