﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using IDotNet;

namespace LZL.IISWeb.Core
{
    public class IISWebElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        public int ID
        {
            get { return base["id"].ToStringEx().ToInt() ?? -1; }
            set { base["id"] = value; }
        }

        [ConfigurationProperty("iisWebSeconds")]
        public IISWebSecondElementCollection WebSeconds
        {
            get
            {
                return (IISWebSecondElementCollection)base["iisWebSeconds"];
            }
        }
    }
}