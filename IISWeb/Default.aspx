﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LZL.IISWeb.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IIS Manage</title>
</head>
<body>
    <h2>IIS Manage</h2>
    <table id="iisList" border="1" cellspacing="0" style="width: 100%">
        <thead></thead>
        <tbody></tbody>
    </table>
    <script src="Scripts/jquery-1.8.3.min.js"></script>
    <script>
        $(function () {
            function request(opt) {
                return $.post("iis.ashx", opt);
            }
            function check(data) {
                var b = data != null && data.errorCode === 0;
                if (!b) {
                    alert(data.msg);
                }
                return b;
            }

            request({ action: 'getAllSites' }).done(function (data) {
                if (check(data)) {
                    var head = [], body = [];
                    $.each(data.data, function (i, item) {
                        if (i == 0) {
                            head.push('<tr>');
                            $.each(item, function (k, v) {
                                head.push('<th>', k, '</th>');
                            });
                            head.push('</tr>');
                        }
                        body.push('<tr>');
                        $.each(item, function (k, v) {
                            var s = v;
                            if (v instanceof Array) {
                                s = [];
                                $.each(v, function (i1, item1) {
                                    var tmpIndex = 0;
                                    $.each(item1, function (k1, v1) {
                                        if (tmpIndex !== 0) {
                                            s.push(' , ');
                                        }
                                        s.push(k1, ':', v1);
                                        tmpIndex++;
                                    });
                                    s.push('<br>');
                                });
                                s = s.join('');
                            }
                            body.push('<td>', s, '</td>');
                        });
                        body.push('</tr>');
                    });
                    $('#iisList').find('>thead').html(head.join('')).end().find('>tbody').html(body.join(''));
                }
            });
        });
    </script>
</body>
</html>
