﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LZL.IISManage;

namespace LZL.IISWeb.Model
{
    public class SiteInfo
    {
        public string Name { get; set; }

        public long Index { get; set; }

        public ServerState? State { get; set; }

        public List<ApplicationInfo> Applications { get; set; }
    }

    public class ApplicationInfo
    {
        public string Path { get; set; }
        public string PoolName { get; set; }
    }
}