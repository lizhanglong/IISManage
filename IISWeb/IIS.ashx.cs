﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Security.Policy;
using System.Web;
using IDotNet;
using LZL;
using LZL.IISWeb.Core;
using LZL.IISWeb.Model;
using LZL.IISManage;

namespace LZL.IISWeb
{
    /// <summary>
    /// IIS 的摘要说明
    /// </summary>
    public class IIS : BaseActionHttpHandler
    {
        private static IDictionary<string, System.Reflection.MethodInfo> services;
        static IIS()
        {
            services = GetServices(typeof(IISApi));
        }
        protected override IDictionary<string, System.Reflection.MethodInfo> Services
        {
            get { return services; }
        }

        protected override BaseActionService ServicesClassType
        {
            get { return new IISApi(Context); }
        }

        public override void ProcessRequest(HttpContext context)
        {
            if (Check(context))
            {
                base.ProcessRequest(context);
            }
        }

        bool Check(HttpContext context)
        {
            var ips = WebConfigHelper.GetAppSettingValue("clientIP").Split(',');
            if (!ips.Contains(context.Request.UserHostAddress))
            {
                context.WriteJson(new IISResultInfo().SetError("Forbidden", 403));
                return false;
            }
            return true;
        }
    }

    class IISApi : BaseActionService
    {
        private IControl control;
        public IISApi(HttpContext context)
            : base(context)
        {
            control = IISControl.GetControl();
        }

        /// <summary>
        /// 获取站点列表
        /// </summary>
        public void GetAllSites()
        {
            var data = control.GetAllSites();
            if (!data.IsSuccess())
            {
                Context.WriteJson(data);
                return;
            }
            var filter = new List<SiteInfo>();
            var sites = IISWebConfig.GetSites();
            foreach (var item in data.data)
            {
                if (!IISWebConfig.HasWebPerm(sites, item.Index)) continue;
                var apps = new List<ApplicationInfo>();
                foreach (var info in item.Applications)
                {
                    if (!IISWebConfig.HasWebPerm(sites, item.Index, info.Path)) continue;
                    apps.Add(new ApplicationInfo()
                    {
                        Path = info.Path,
                        PoolName = info.PoolName
                    });
                }
                if (apps.Count > 0)
                {
                    filter.Add(new SiteInfo()
                    {
                        Name = item.Name,
                        Index = item.Index,
                        State = item.State,
                        Applications = apps
                    });
                }
            }
            Context.WriteJson(new IISResultInfo() { data = filter });
        }

        /// <summary>
        /// 回收应用程序池
        /// </summary>
        public void RecyleAppPool()
        {
            var data = control.GetAllSites();
            if (!data.IsSuccess())
            {
                Context.WriteJson(data);
                return;
            }
            var appPoolName = Request.Form["appPoolName"];
            if (!IISWebConfig.HasAppPoolConfigPerm(data.data, appPoolName))
            {
                Context.WriteJson(new IISResultInfo().SetError("没有权限", 403));
                return;
            }
            var data1 = control.RecycleAppPool(appPoolName);
            Context.WriteJson(data1);
        }
        /// <summary>
        /// 重启网站
        /// </summary>
        public void ReStartSite()
        {
            var siteId = Request.Form["siteId"].ToLong() ?? -1;
            var sites = IISWebConfig.GetSites();
            if (!IISWebConfig.HasWebPerm(sites, siteId))
            {
                Context.WriteJson(new IISResultInfo().SetError("没有权限", 403));
                return;
            }
            var data = control.ReStartSite(new IISStartInfo()
            {
                ParentIndex = siteId
            });
            Context.WriteJson(data);
        }
    }

}