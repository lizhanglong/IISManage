﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using IDotNet;
using Microsoft.Web.Administration;

namespace LZL.IISManage
{
    internal class IIS7Control : BaseControl
    {
        public override IISResultInfo<List<IISAppPoolInfo>> GetAllPoolInfos()
        {
            var data = new IISResultInfo<List<IISAppPoolInfo>>();
            var list = new List<IISAppPoolInfo>();
            try
            {
                var iisManager = new ServerManager();
                foreach (var item in iisManager.ApplicationPools)
                {
                    list.Add(new IISAppPoolInfo()
                    {
                        Name = item.Name,
                        Enable32BitAppOnWin64 = item.Enable32BitAppOnWin64,
                        ManagedRuntimeVersion = item.ManagedRuntimeVersion,
                        State = (AppPoolState)((int)item.State),
                        AppPoolAutoStart = item.AutoStart,
                        //WorkerProcessesCount = item.WorkerProcesses.LongCount(),
                        // MaxProcessesCount = item.ProcessModel.MaxProcesses,
                        IdentityType = item.ProcessModel.IdentityType.ToString().ToEnum<AppPoolIdentityType>(),
                        AnonymousUserName = item.ProcessModel.UserName
                    });
                }
                iisManager.Dispose();
                data.data = list;
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }


        public override IISResultInfo<List<IISSiteInfo>> GetAllSites()
        {
            var data = new IISResultInfo<List<IISSiteInfo>>();
            var list = new List<IISSiteInfo>();
            try
            {
                using (ServerManager iisManager = new ServerManager())
                {
                    foreach (var item in iisManager.Sites)
                    {
                        list.Add(new IISSiteInfo()
                        {
                            Name = item.Name,
                            Index = item.Id,
                            ServerAutoStart = item.ServerAutoStart,
                            ServerBindings = item.GetServerBindings(),
                            DefaultDoc = item.GetWebConfiguration().GetSection("system.webServer/defaultDocument").GetCollection("files").Select(p => p["value"]).StringJoin(","),
                            State = item.State.ToServerState(),
                            Applications = new List<SiteApplicationInfo>(
                                item.Applications.Select(p =>
                                    new SiteApplicationInfo()
                                    {
                                        Path = p.Path.TrimStart('/'),
                                        PoolName = p.ApplicationPoolName
                                    })),
                        });
                    }
                }
                data.data = list;
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }



        public override IISResultInfo<object> StartSite(IISStartInfo info)
        {
            var data = new IISResultInfo();
            try
            {
                using (var iisManager = new ServerManager())
                {
                    var site = iisManager.Sites.FirstOrDefault(p => p.Id == info.ParentIndex);
                    if (site == null)
                    {
                        return data.SetError("site not found", 404);
                    }
                    switch (site.State)
                    {
                        case ObjectState.Starting:
                            break;
                        case ObjectState.Started:
                            break;
                        case ObjectState.Stopping:
                        case ObjectState.Stopped:
                        case ObjectState.Unknown:
                            site.Start();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }

        public override IISResultInfo<object> StopSite(IISStartInfo info)
        {
            var data = new IISResultInfo();
            try
            {
                using (var iisManager = new ServerManager())
                {
                    var site = iisManager.Sites.FirstOrDefault(p => p.Id == info.ParentIndex);
                    if (site == null)
                    {
                        return data.SetError("site not found", 404);
                    }
                    switch (site.State)
                    {
                        case ObjectState.Starting:
                        case ObjectState.Started:
                            site.Stop();
                            break;
                        case ObjectState.Stopping:
                        case ObjectState.Stopped:
                        case ObjectState.Unknown:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }

        public override IISResultInfo<object> StartAppPool(string appPoolName)
        {
            var data = new IISResultInfo();
            try
            {
                using (var iisManager = new ServerManager())
                {
                    var appPool = iisManager.ApplicationPools.FirstOrDefault(p => p.Name == appPoolName);
                    if (appPool == null)
                    {
                        return data.SetError("appPool not found", 404);
                    }
                    switch (appPool.State)
                    {
                        case ObjectState.Starting:
                            break;
                        case ObjectState.Started:
                            break;
                        case ObjectState.Stopping:
                        case ObjectState.Stopped:
                        case ObjectState.Unknown:
                            appPool.Start();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }

        public override IISResultInfo<object> StopAppPool(string appPoolName)
        {
            var data = new IISResultInfo();
            try
            {
                using (var iisManager = new ServerManager())
                {
                    var appPool = iisManager.ApplicationPools.FirstOrDefault(p => p.Name == appPoolName);
                    if (appPool == null)
                    {
                        return data.SetError("appPool not found", 404);
                    }
                    switch (appPool.State)
                    {
                        case ObjectState.Starting:
                        case ObjectState.Started:
                            appPool.Stop();
                            break;
                        case ObjectState.Stopping:
                        case ObjectState.Stopped:
                        case ObjectState.Unknown:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }

        public override IISResultInfo<object> RecycleAppPool(string appPoolName)
        {

            var data = new IISResultInfo();
            try
            {
                using (var iisManager = new ServerManager())
                {
                    var appPool = iisManager.ApplicationPools.FirstOrDefault(p => p.Name == appPoolName);
                    if (appPool == null)
                    {
                        return data.SetError("appPool not found", 404);
                    }
                    switch (appPool.State)
                    {
                        case ObjectState.Starting:
                            break;
                        case ObjectState.Started:
                            appPool.Recycle();
                            break;
                        case ObjectState.Stopping:
                        case ObjectState.Stopped:
                        case ObjectState.Unknown:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception e)
            {
                data.SetError(e);
            }
            return data;
        }
    }
}
