﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public enum AppPoolState
    {
        Starting = 0,
        Started,
        Stopping,
        Stopped,
        Unknown
    }
}
