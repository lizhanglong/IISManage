﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public class IISSiteInfo
    {
        public string Name { get; set; }
        public long Index { get; set; }
        public ServerState? State { get; set; }
        public bool? ServerAutoStart { get; set; }
        public string ServerBindings { get; set; }
        public string DefaultDoc { get; set; }
        //public string AnonymousUserName { get; set; }

        //public bool EnableDirBrowsing { get; set; }
        // public string ServerComment { get; set; }

        // public string DefaultApplicationPoolName { get; set; }

        public List<SiteApplicationInfo> Applications { get; set; }
    }
}
