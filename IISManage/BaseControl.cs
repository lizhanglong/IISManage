﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    internal abstract class BaseControl : IControl
    {
        public abstract IISResultInfo<List<IISAppPoolInfo>> GetAllPoolInfos();

        public abstract IISResultInfo<List<IISSiteInfo>> GetAllSites();

        public virtual IISResultInfo<object> ReStartSite(IISStartInfo info)
        {
            var ret = StopSite(info);
            if (ret.IsSuccess())
            {
                ret = StartSite(info);
            }
            return ret;
        }

        public abstract IISResultInfo<object> StartSite(IISStartInfo info);

        public abstract IISResultInfo<object> StopSite(IISStartInfo info);


        public abstract IISResultInfo<object> RecycleAppPool(string appPoolName);

        public abstract IISResultInfo<object> StartAppPool(string appPoolName);

        public abstract IISResultInfo<object> StopAppPool(string appPoolName);
    }
}
