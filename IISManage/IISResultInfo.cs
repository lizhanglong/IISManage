﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public class IISResultInfo<T>
    {
        public int errorCode { get; set; }

        public string msg { get; set; }

        public bool IsSuccess()
        {
            return errorCode == 0;
        }

        public IISResultInfo<T> SetError(string msg, int errorCode = 1)
        {
            this.msg = msg;
            this.errorCode = errorCode;
            return this;
        }
        public IISResultInfo<T> SetError(Exception exception, int errorCode = 1)
        {
            this.msg = exception.Message;
            this.errorCode = errorCode;
            return this;
        }

        public T data { get; set; }
    }
    public class IISResultInfo : IISResultInfo<object>
    {
    }
}
