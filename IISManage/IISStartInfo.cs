﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public class IISStartInfo
    {
        public string Path { get; set; }

        public long ParentIndex { get; set; }
    }
}
