﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public class IISControl
    {
        public static IControl GetControl()
        {
            if (IISHelper.IsIIS7AndHigher())
            {
                return new IIS7Control();
            }
            return new IIS6Control();
        }
        public static IControl GetControl(bool IsIIS7AndHigher)
        {
            if (IsIIS7AndHigher)
            {
                return new IIS7Control();
            }
            return new IIS6Control();
        }
    }
}
