﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace LZL.IISManage
{
    public interface IControl
    {
        IISResultInfo<List<IISAppPoolInfo>> GetAllPoolInfos();

        IISResultInfo<List<IISSiteInfo>> GetAllSites();

        IISResultInfo<object> ReStartSite(IISStartInfo info);

        IISResultInfo<object> StartSite(IISStartInfo info);

        IISResultInfo<object> StopSite(IISStartInfo info);

        IISResultInfo<object> RecycleAppPool(string appPoolName);

        IISResultInfo<object> StartAppPool(string appPoolName);

        IISResultInfo<object> StopAppPool(string appPoolName);
    }
}
