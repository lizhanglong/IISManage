﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public enum AppPoolIdentityType
    {
        LocalSystem,
        LocalService,
        NetworkService,
        SpecificUser,
        ApplicationPoolIdentity
    }
}
