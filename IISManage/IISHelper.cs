﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using Microsoft.Web.Administration;

namespace LZL.IISManage
{
    public class IISHelper
    {

        public static bool IsIIS7AndHigher()
        {
            #region 操作系统
            /*****************************************************************************
Operating System             Version     PlatformID
Windows 8                    6.2         VER_PLATFORM_WIN32_NT (=2)
Windows 7                    6.1         VER_PLATFORM_WIN32_NT
Windows Server 2008 R2       6.1         VER_PLATFORM_WIN32_NT
Windows Server 2008          6.0         VER_PLATFORM_WIN32_NT
Windows Vista                6.0         VER_PLATFORM_WIN32_NT
Windows Server 2003 R2       5.2         VER_PLATFORM_WIN32_NT
Windows Server 2003          5.2         VER_PLATFORM_WIN32_NT
Windows XP 64-Bit Edition    5.2         VER_PLATFORM_WIN32_NT
Windows XP                   5.1         VER_PLATFORM_WIN32_NT
Windows 2000                 5.0         VER_PLATFORM_WIN32_NT
Windows NT 4.0               4.0         VER_PLATFORM_WIN32_NT
Windows NT 3.51              3.51 ?      VER_PLATFORM_WIN32_NT
Windows Millennium Edition   4.90        VER_PLATFORM_WIN32_WINDOWS (=1)
Windows 98                   4.10        VER_PLATFORM_WIN32_WINDOWS
Windows 95                   4.0         VER_PLATFORM_WIN32_WINDOWS
Windows 3.1                  3.1 ?       VER_PLATFORM_WIN32s (=0)
*****************************************************************************/
            #endregion

            return Environment.OSVersion.Version.Major > 5;
        }

        public static string GetIISVersion(string domainname = "LOCALHOST")
        {
            try
            {
                var getEntity = new DirectoryEntry("IIS://" + domainname + "/W3SVC/INFO");
                return getEntity.Properties["MajorIISVersionNumber"].Value.ToString();
            }
            catch (Exception e)
            {
                Trace.TraceError("获取IIS版本时抛出异常：{0}", e.Message);
                return null;
            }
        }
    }
}
