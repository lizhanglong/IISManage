﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    internal class Test
    {
        public static void ShowProperties(PropertyCollection props)
        {
            var sb = new StringBuilder();
            foreach (string key in props.PropertyNames)
            {
                sb.AppendFormat("{0}={1}{2}", key, props[key].Value, Environment.NewLine);
            }
            //   Console.WriteLine(sb.ToString());
            Trace.WriteLine(sb.ToString());
        }
    }
}
