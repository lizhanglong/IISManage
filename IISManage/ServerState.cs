﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LZL.IISManage
{
    public enum ServerState
    {
        Starting =1
        ,
        Started
        ,
        Stopping
        ,
        Stopped
        ,
        Pausing
        ,
        Paused
        ,
        Continuing
    }
}
