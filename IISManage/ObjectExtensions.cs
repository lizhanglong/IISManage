﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using IDotNet;
using Microsoft.Web.Administration;

namespace LZL.IISManage
{
    public static class ObjectExtensions
    {

        public static string GetFirstValue(this PropertyValueCollection vals, string isNullValue = null)
        {
            if (vals == null || vals.Count == 0)
            {
                return isNullValue;
            }
            return vals[0].ToString();
        }
        public static string[] GetAllValue(this PropertyValueCollection vals)
        {
            if (vals == null)
            {
                return null;
            }
            var arr = new string[vals.Count];
            for (int i = 0, length = vals.Count; i < length; i++)
            {
                arr[i] = vals[i].ToString();
            }
            return arr;
        }

        public static ServerState ToServerState(this ObjectState state)
        {
            var t = (int)state;
            return (ServerState)(t + 1);
            // return state.ToString("d").ToEnum<ServerState>();
        }

        public static string GetServerBindings(this Site site)
        {
            try
            {
                var list = new List<string>();
                foreach (var p in site.Bindings)
                {
                    if (p.EndPoint==null)
                    {
                        continue;
                    }
                    list.Add(":{0}:{1}".FormatWith(p.EndPoint.Port, p.Host));
                }
                return list
                        .StringJoin(",");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
